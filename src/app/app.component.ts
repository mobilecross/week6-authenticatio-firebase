import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import firebase from 'firebase';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { SkripsiPage } from '../pages/skripsi/skripsi';
import { SettingsPage } from '../pages/settings/settings';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { AuthService } from '../services/authService';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;
  tabsPage = TabsPage;
  loginPage = LoginPage;
  signupPage = SignupPage;
  settingsPage = SettingsPage;
  checked = false;
  @ViewChild('sideMenuContent') nav:NavController;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private menuCtrl:MenuController,
    private authCtrl : AuthService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    firebase.initializeApp({
      apiKey: "AIzaSyAOatsDREXS_AoNVM8nLNFSz0nU3UXYEZ0",
      authDomain: "quotes-b8f96.firebaseapp.com"
      });
  
    firebase.auth().onAuthStateChanged(user => {
      if(user) {
        //do something here if the user is logged in
        this.checked = true;
      }
      else {
        //do something here if the user is not logged in
        this.checked = false;
      }
    });
  }
  
  check(){
    return this.checked;
  }
  
  onLoad(page:any){
    this.nav.setRoot(page);
    this.menuCtrl.close;
  }

  logout(){
    this.authCtrl.logout();
    console.log("logout");
  }
}

