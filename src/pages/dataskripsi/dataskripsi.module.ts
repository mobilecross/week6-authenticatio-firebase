import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DataskripsiPage } from './dataskripsi';

@NgModule({
  declarations: [
    DataskripsiPage,
  ],
  imports: [
    IonicPageModule.forChild(DataskripsiPage),
  ],
})
export class DataskripsiPageModule {}
