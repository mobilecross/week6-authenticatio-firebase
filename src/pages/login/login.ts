import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController  } from 'ionic-angular';
import { NgForm } from '../../../node_modules/@angular/forms';
import { AuthService } from '../../services/authService';
import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnInit{
  tabsPage = TabsPage;
  errorx: string;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private authCtrl : AuthService) {
  }

  ngOnInit() {
    console.log('ionViewDidLoad LoginPage');
  }


  tryLogin(f:NgForm){
    console.log(f.value);
     let data = f.value;
     if(!data.email){
       return;
     }

     this.authCtrl.signin(f.value.email, f.value.pass);
     console.log("login");
     this.navCtrl.push(this.tabsPage);

  }


}
