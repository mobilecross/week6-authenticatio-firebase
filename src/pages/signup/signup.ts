import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController  } from 'ionic-angular';
import { NgForm } from '../../../node_modules/@angular/forms';
import { AuthService } from '../../services/authService';
import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage implements OnInit{

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private authCtrl:AuthService) {
  }

  ngOnInit() {
    console.log('ionViewDidLoad SignUpPage');
  }


  trySignup(f:NgForm){
    console.log(f.value);
    this.authCtrl.signup(f.value.email, f.value.pass);
    console.log("new account");
    this.navCtrl.push(TabsPage);
    // console.log(this.isChecked);
    //console.log(f.value.nama);
    // let modal = this.modalCtrl.create(DataskripsiPage,{nama:f.value.nama, nim:f.value.nim, judul: f.value.judul, kelas:f.value.optClass,nama1:f.value.nama1, nama2:f.value.nama2,linpro:this.isChecked});
    // modal.present();
  }

}
