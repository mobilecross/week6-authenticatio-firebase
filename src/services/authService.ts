import firebase from 'firebase';

export class AuthService{
private user : firebase.User;

signup(email: string, password: string) {
return firebase.auth().createUserWithEmailAndPassword(email,password);
}

signin(email:string, password: string) {
return firebase.auth().signInWithEmailAndPassword(email, password);
}

logout() {
firebase.auth().signOut();
}

get authenticated() : boolean{
    return this.user !== null;
}

getActiveUser () {
    return firebase.auth().currentUser;
    }

}